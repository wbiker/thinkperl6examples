#!/usr/bin/env perl6

#| Exercise 3.1
multi sub MAIN(Str $string) {
    say " " x 70 - $string.chars, $string;
}

#| Exercise 3.3
multi sub MAIN('grid') {
    my $seperator = "+ - - - - + - - - - +";
    for 1..11 {
        if $_ == 1 or $_ == 6 or $_ == 11 {
            say $seperator;
            next;
        }
        say "|         |         |";
    }
}

#| Exercise 3.2
multi sub MAIN() {
    do-four(&print-twice, "Whats up doc");
}

sub do-twice($code, $value) {
    $code($value);
    $code($value);
}

sub do-four($code, $value) {
    do-twice($code, $value);
    #do-twice($code, $value);
}

sub print-twice($value) {
    say $value;
    say $value;
}

sub greet {
    say "Hello World!";
}
