#!/usr/bin/env perl6


sub MAIN() {
    my $sec = prompt("Please enter seconds:" );
    calculate-time($sec);
}

sub calculate-time($seconds) {
    my ($secs, $min, $h, $d) = $seconds.Int.polymod(60, 60, 24);

    say "$d days $h hours $min minutes $secs seconds";

    my $sec = $seconds;
    my $days = ($sec / (24*60*60)).Int;
    my $remainder = $sec % (24*60*60);
    my $hours = ($remainder / (60 * 60)).Int;
    $remainder = $sec % (60*60);
    my $minutes = ($remainder / 60).Int;
    $sec = $sec % 60;

    say "$days:$hours:$minutes:$sec";

    say 2*24*60*60;
    say 18*60*60;
    say 40*60;
    say (2*24*60*60) + (18*60*60) + (40*60);
}

calculate-time(240000);